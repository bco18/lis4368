

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema bco18
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `bco18` ;

-- -----------------------------------------------------
-- Schema bco18
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `bco18` DEFAULT CHARACTER SET utf8mb4 ;
SHOW WARNINGS;
USE `bco18` ;

-- -----------------------------------------------------
-- Table `bco18`.`petstore`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bco18`.`petstore` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `bco18`.`petstore` (
  `pst_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pst_name` VARCHAR(30) NOT NULL,
  `pst_street` VARCHAR(30) NOT NULL,
  `pst_city` VARCHAR(30) NOT NULL,
  `pst_state` CHAR(2) NOT NULL,
  `pst_zip` CHAR(9) NOT NULL,
  `pst_phone` BIGINT UNSIGNED NOT NULL,
  `pst_email` VARCHAR(100) NOT NULL,
  `pst_url` VARCHAR(100) NOT NULL,
  `pst_ytd_sales` DECIMAL(10,2) NOT NULL,
  `pst_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pst_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `bco18`.`customer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bco18`.`customer` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `bco18`.`customer` (
  `cus_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `cus_fname` VARCHAR(15) NOT NULL,
  `cus_lname` VARCHAR(30) NOT NULL,
  `cus_street` VARCHAR(30) NOT NULL,
  `cus_city` VARCHAR(30) NOT NULL,
  `cus_state` CHAR(2) NOT NULL,
  `cus_zip` CHAR(9) NOT NULL,
  `cus_phone` BIGINT UNSIGNED NOT NULL,
  `cus_email` VARCHAR(100) NOT NULL,
  `cus_balance` DECIMAL(8,2) NOT NULL,
  `cus_total_sales` DECIMAL(8,2) NOT NULL,
  `cus_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`cus_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `bco18`.`pet`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bco18`.`pet` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `bco18`.`pet` (
  `pet_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pst_id` SMALLINT UNSIGNED NOT NULL,
  `cus_id` SMALLINT UNSIGNED NULL,
  `pet_type` VARCHAR(45) NOT NULL,
  `pet_sex` ENUM('m', 'f') NOT NULL,
  `pet_cost` DECIMAL(6,2) NOT NULL,
  `pet_price` DECIMAL(6,2) NOT NULL,
  `pet_age` TINYINT NOT NULL,
  `pet_color` VARCHAR(30) NOT NULL,
  `pet_sale_date` DATE NULL,
  `pet_vaccine` ENUM('y', 'n') NOT NULL,
  `pet_neuter` ENUM('y', 'n') NOT NULL,
  `pet_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pet_id`),
  INDEX `fk_pet_customer_idx` (`cus_id` ASC) VISIBLE,
  INDEX `fk_pet_petstore1_idx` (`pst_id` ASC) VISIBLE,
  CONSTRAINT `fk_pet_customer`
    FOREIGN KEY (`cus_id`)
    REFERENCES `bco18`.`customer` (`cus_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_pet_petstore1`
    FOREIGN KEY (`pst_id`)
    REFERENCES `bco18`.`petstore` (`pst_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `bco18`.`petstore`
-- -----------------------------------------------------
START TRANSACTION;
USE `bco18`;
INSERT INTO `bco18`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Pets R Us', '997 Leaf Valley', 'Wichita Falls', 'NM', '432742389', 2038442657, 'zamora8767@ftml.net ', 'pets.com', 654.03, NULL);
INSERT INTO `bco18`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'We Like Pets', '319 Colonial Common', 'Muskego', 'PA', '536742189', 4695813376, 'russell7774@xsmail.com', 'snakes.com', 342.12, NULL);
INSERT INTO `bco18`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Snakes 4 Hire', '120 Colonial Sixth', 'Conley', 'IN', '232742189', 5438045840, 'groenendijk4334@inoutbox.com', 'tamedog.com', 432.21, NULL);
INSERT INTO `bco18`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Birds Are Fun', '95 Squaw Quay', 'Moraine', 'ID', '732352389', 3413241544, 'kelly5767@fastmail.co.uk', 'welikedogs.com', 231.65, NULL);
INSERT INTO `bco18`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Dog Sale', '573 Gentle Path', 'Butterfield Village', 'CT', '522742789', 4524524676, 'rhea9142@fastemailer.com', 'cats.com', 454.57, NULL);
INSERT INTO `bco18`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Cats R Mean', '685 Dale Tunnel', 'Westmost Village', 'NH', '832748359', 5363568553, 'randall3988@mailup.net', 'yahoo.com', 564.76, NULL);
INSERT INTO `bco18`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Time for Pets', 'Rose Stravenue', 'Rumson Borough', 'UT', '532721359', 3453457465, 'aldo_haak7607@postpro.net', 'petsrus.com', 876.98, NULL);
INSERT INTO `bco18`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'PetsMinus', '747 Tawny Boulevard', 'Shelton', 'MN', '132743376', 6234524545, 'groenendijk4334@inoutbox.com', 'catsrmean.com', 457.87, NULL);
INSERT INTO `bco18`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'PetsAdd', '472 Little Lake', 'Fairfield', 'CT', '413413743', 2452524537, 'terina760@mailup.net', 'petsadd.com', 456.34, NULL);
INSERT INTO `bco18`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Ferrets R Us', '548 Big Lake', 'Trumbull', 'NJ', '342857686', 2452457876, 'stephan4959@mail-page.com', 'ferretsrus.com', 342.234, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `bco18`.`customer`
-- -----------------------------------------------------
START TRANSACTION;
USE `bco18`;
INSERT INTO `bco18`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Sandy', 'Lutz', '174 Brook Meadow', 'East Bank', 'WY', '765460409', 4534395403, 'sandylutz@gmail.com', 412.43, 6757.65, NULL);
INSERT INTO `bco18`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Britney', 'Kleijn', '302 Kennedy Ave', 'Shiloh', 'ID', '132145365', 2313154356, 'nathan_byers2601@fmgirl.com', 2312.43, 6756.42, NULL);
INSERT INTO `bco18`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Milo', 'Albert', '438 Lost Sky', 'McGill', 'NM', '563467786', 5346345654, 'dougherty4675@inoutbox.com', 5435.23, 3452.12, NULL);
INSERT INTO `bco18`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Dedra', 'Brown', '704 Field Highlands', 'Lone Tree', 'NY', '231236433', 3241344563, 'pols6583@the-quickest.com', 2312.43, 2134.45, NULL);
INSERT INTO `bco18`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Andre', 'Dixon', '43 Brownie Rd', 'West Bank', 'FL', '453453766', 4534634565, 'harley8543@fastimap.com', 5437.67, 3664.56, NULL);
INSERT INTO `bco18`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Sydney', 'Russel', '196 Stony Vale', 'Irmo', 'DE', '231267474', 4352452433, 'vennie274@mailc.net', 4563.76, 6768.67, NULL);
INSERT INTO `bco18`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Brennan', 'Murillo', '234 Meadowlark Dr', 'Mayo', 'CO', '564598763', 7665754345, 'keneth5269@sent.at', 7653.87, 6768.84, NULL);
INSERT INTO `bco18`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Sarah', 'Bosschart', '439 Northwest Ave', 'Mullinville', 'WI', '456346346', 7657465645, 'shenna4020@internet-mail.org', 2526.65, 4356.23, NULL);
INSERT INTO `bco18`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Rudolph', 'van Ree', '902 Southeast Ave', 'Bradfurd', 'WA', '346537656', 3456457765, 'bernie_boltendal2126@fastmail.us', 3451.43, 3432.54, NULL);
INSERT INTO `bco18`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Woodrow', 'Oranje', '91 Capital Circle', 'Pomaria', 'LA', '765476677', 5645645222, 'suzi7069@proinbox.com', 2342.76, 6452.32, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `bco18`.`pet`
-- -----------------------------------------------------
START TRANSACTION;
USE `bco18`;
INSERT INTO `bco18`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 3, 2, 'Long Tail', 'f', 254.32, 765.67, 13, 'Tan', '2020-03-13', 'y', 'y', NULL);
INSERT INTO `bco18`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 4, 2, 'Havanese', 'm', 234.54, 657.45, 2, 'Brown', '2021-12-23', 'y', 'y', NULL);
INSERT INTO `bco18`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 5, 6, 'Maltese', 'f', 54.23, 564.23, 12, 'White', '2019-11-30', 'y', 'y', NULL);
INSERT INTO `bco18`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 6, 3, 'Sheepdog', 'm', 344.54, 324.65, 30, 'Black', NULL, 'y', 'n', NULL);
INSERT INTO `bco18`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 7, 3, 'English Bulldog', 'm', 425.31, 346.67, 54, 'Green', NULL, 'y', 'n', NULL);
INSERT INTO `bco18`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 8, 4, 'American Bulldog', 'f', 231.54, 768.87, 32, 'Maroon', NULL, 'y', 'n', NULL);
INSERT INTO `bco18`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 9, 9, 'Goldfish', 'm', 265.89, 745.67, 12, 'Violet', '2018-04-23', 'n', 'n', NULL);
INSERT INTO `bco18`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 10, 7, 'Mastiff', 'm', 564.78, 675.67, 32, 'Magenta', '2018-06-21', 'y', 'y', NULL);
INSERT INTO `bco18`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 1, 1, 'Pitbull', 'm', 667.76, 643.64, 21, 'Blue', NULL, 'y', 'y', NULL);
INSERT INTO `bco18`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 2, NULL, 'Shepard', 'f', 654.76, 312.43, 21, 'Green', NULL, 'n', 'y', NULL);

COMMIT;
