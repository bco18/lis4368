
# LIS4368 - Advanced Web Application Development

## Brennan O'Hara

### Assignment 3 Requirements:

*Three Parts:*

1. Create Pet Store ERD using mySQL
2. Fill each table with 10 records
3. Update a3/index.jsp


#### README.md file should include the following items:


1. Screenshot of Pet Store ERD
2. Screenshot of 10 records in each table
3. Update of a3/index.jsp
4. Links to a3.mwb and a3.sql files


#### Assignment Screenshots:

*Screenshot of A3 ERD:*

![A3 ERD](img/a3_ERD.PNG "ERD based upon A3 Requirements")

*GIF of Table Records:*

![A2 Index](https://media.giphy.com/media/icsTtLcWO4v1juIY5D/giphy.gif)

*Links to a3.mwb and a3.sql files:*

[A3 MWB File](docs/a3.mwb "A3 ERD in .mwb format")

[A3 SQL File](docs/a3.sql "A3 SQL Script")

#### GIF of Skillsets 4,5,6:

![Skillsets 4, 5, 6](https://media.giphy.com/media/iypt7YA9ns5YCamvO8/giphy.gif)
