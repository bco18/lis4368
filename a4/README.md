
# LIS4368 - Advanced Web Application Development

## Brennan O'Hara

### Assignment 4 Requirements:

*Three Parts:*

1. Modify thanks.jsp and customerform.jsp
2. Correctly compile customerAdmin and Customer.java
3. Show GIF of passed and failed server-side validation


#### README.md file should include the following items:

1. GIF of customerAdmin Servlet
2. Suitably modify thanks.jsp and customerform.jsp
3. Correctly compile Customer.java and customerAdmin
4. Skillsets 10, 11, and 12


#### Assignment Screenshots:

*GIF of A4 customerAdmin Servlet:*

![A4 Servlet](https://media.giphy.com/media/dNljZaG9t5UnJQ4H8K/giphy.gif)


#### GIF of Skillsets 10,11,12:

![Skillsets 10,11,12](https://media.giphy.com/media/htLhk6hrukU0tSH4SV/giphy.gif)
