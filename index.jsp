<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that shows completed work in LIS4368- Advanced Web Application Development.">
	<meta name="author" content="Brennan C O'Hara">
	<link rel="icon" href="ic_launcher.png">

	<title>My Online Portfolio</title>

	<%@ include file="/css/include_css.jsp" %>		

<!-- Carousel styles -->
<style type="text/css">
h1
		 {
			 margin: 0;     
			 color: #4b8efd;
			 padding-top: 40px;
			 padding-bottom: 40px;
			 font-size: 48px;
			 font-family: "trebuchet ms", sans-serif; 
			 text-shadow: 3px 3px #bdbdbd
		 }
		 h2
		 {
			 margin: 0;     
			 color: #ffffff;
			 padding-top: 25px;
			 font-size: 52px;
			 font-family: "trebuchet ms", sans-serif;
		 }    
.item
{
	background: #4b8efd;    
	text-align: center;
	height: 300px !important;
}
.container
{
	text-align: center;
}
.carousel
{
  margin: 20px 0px 20px 0px;
}
.bs-example
{
  margin: 20px;
}
</style>
	
</head>
<body>
	
	<%@ include file="/global/nav_global.jsp" %>	
	
	<div class="container">
		 <div class="starter-template">
						<div class="page-header">
						<%@ include file="/global/header.jsp" %>							
						</div>

<!-- Start Bootstrap Carousel  -->
<div class="bs-example">
	<div
      id="myCarousel"
		class="carousel"
		data-interval="1000"
		data-pause="hover"
		data-wrap="true"
		data-keyboard="true"			
		data-ride="carousel">
		
    	<!-- Carousel indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>   
       <!-- Carousel items -->
        <div class="carousel-inner">

				 <div class="active item" style="background: url(img/cyber.png) no-repeat left center; background-size: cover;">
					<a href="https://www.linkedin.com/in/bcohara">
						<h2>Brennan O'Hara's LinkedIn</h2>
						 <div class="carousel-caption">
							 <p class="lead">Keep up with me on LinkedIn.</p>
						 </div>
					 </a>
				 </div>					

         <div class="item" style="background: url(img/python.png) no-repeat left center; background-size: cover;">
			<a href="https://bitbucket.org/bco18/lis4369">
                <h2>Extensible Enterprise Solutions</h2>
                <div class="carousel-caption">
                  <p class='lead'>Uses Python, R, and my SQL</p>
						 <!--  <img src="img/slide2.png" alt="Slide 2">									 -->						
                </div>
				</a>
            </div>

         <div class="item" style="background: url(img/webapp.png) no-repeat left center; background-size: cover;">
				<a href="https://bitbucket.org/bco18/lis4381">
                <h2>Mobile Web Application Development</h2>
                <div class="carousel-caption">
                  <p class='lead'>Uses XML, Java, CSS, and PHP.</p>
						<!--  <img src="img/slide3.png" class="img-responsive" alt="Slide 3">							 -->								
                </div>
				</a>
            </div>

        </div>
        <!-- Carousel nav -->
        <a class="carousel-control left" href="#myCarousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <a class="carousel-control right" href="#myCarousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
    </div>
</div>
<!-- End Bootstrap Carousel  -->

 	<%@ include file="/global/footer.jsp" %>

	</div> <!-- end starter-template -->
</div> <!-- end container -->

 	<%@ include file="/js/include_js.jsp" %>
	
</body>
</html>
