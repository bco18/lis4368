# LIS4368 - Advanced Web Application Development

## Brennan O'Hara

### Assignment 1 Requirements:

*Four Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Java/Tomcat Installation
3. Chapter Questions (Chs. 1, 2)


#### README.md file should include the following items:

* Screenshot of java Hello
* Screenshot of running http://localhost:9999
* Git commands with short descriptions
* Bitbucket repo links:

    a) this assignment and

    b) the completed tutorial (bitbucketstationlocations).


#### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. git branch - List, create, or delete branches


#### Assignment Screenshots:

*GIF running java Hello*:

![Javac Hello World!](https://media.giphy.com/media/PHGflcFrOfJgoGSa0e/giphy.gif)

*Screenshot running http://localhost:9999*:

![Tomcat Installation Screenshot](img/tomcat.PNG)

*GIF of a1/index.jsp*:

![A1 Index.jsp](https://media.giphy.com/media/vj2bDtPKGsVJwz1tfB/giphy.gif)




#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/bco18/bitbucketstationlocations/ "Bitbucket Station Locations")