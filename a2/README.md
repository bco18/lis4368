
# LIS4368 - Advanced Web Application Development

## Brennan O'Hara

### Assignment 2 Requirements:

*Four Parts:*

1. Install MySQL using HomeBrew
2. Create Database Servlet
3. Update a2/index.jsp


#### README.md file should include the following items:

1. Links to: 
    * <http://localhost:9999/hello>
    * <http://localhost:9999/hello/HelloHome.html>
    * <http://localhost:9999/hello/sayhello>
    * <http://localhost:9999/hello/querybook.html>
2. GIF of:
    * querybook.html
    * query results
    * a2/index.jsp
3. Link to LIS4368 Site: <http://localhost:9999/lis4368>


#### Assignment Screenshots:

*GIF Java Servlet*:

![Java Servlet](https://media.giphy.com/media/PgP6W4SyxwNNZVaizA/giphy.gif)

*GIF of A2 Index:*

![A2 Index](https://media.giphy.com/media/jFxNzMjkhHikNnl4uk/giphy.gif)

#### GIF of Skillsets 1,2,3:

![Skillsets 1, 2, 3](https://media.giphy.com/media/f10mpinp1zHFRiyQin/giphy.gif)
