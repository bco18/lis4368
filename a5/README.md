
# LIS4368 - Advanced Web Application Development

## Brennan O'Hara

### Assignment 5 Requirements:

*Three Parts:*

1. Modify data subdirectory as well as customerServlet.java
2. Correctly compile DB files from data subdirectory
3. Show GIF of passed and failed server-side validation with SQL insertion


#### README.md file should include the following items:

1. GIF of pre-validation and post validation
2. Show SQL insertion using command line
3. Skillsets 13, 14, 15


#### Assignment Screenshots:

*GIF of A5 pre and post validation w/ SQL insertion:*

![A5 Servlet](https://media.giphy.com/media/u2KbdipWpbufHLyl1x/giphy.gif)


#### GIF of Skillsets 10,11,12:

![Skillsets 13,14,15](https://media.giphy.com/media/0Bfn20rj8Rb69h13et/giphy.gif)
