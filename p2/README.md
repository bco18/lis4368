
# LIS4368 - Advanced Web Application Development

## Brennan O'Hara

### Project 2 Requirements:

*Three Parts:*

1. Implement modify and delete functionality
2. Show Failed Validation / Successful Validation for each
3. Demonstrate MVC with client and server-side validation

#### README.md file should include the following items:


1. GIF update, add, and delete functionality
2. SQL terminal pre and post command


#### Assignment Screenshots:

*Screenshot of P1 Validation:*

![P2 WebApp](https://media.giphy.com/media/JSJeH0JatldO0Hf4Dk/giphy.gif)

*GIF of SQL terminal Pre and Post command*

![P2 SQL](https://media.giphy.com/media/4yHxJd4tEykHKamCI0/giphy.gif)
