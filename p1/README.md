
# LIS4368 - Advanced Web Application Development

## Brennan O'Hara

### Project 1 Requirements:

*Three Parts:*

1. Demonstrate use of JQuery to validate client side data
2. Show Failed Validation / Successful Validation
3. Skillsets 7, 8, and 9

#### README.md file should include the following items:


1. GIF of Failed/Successful data validation
2. Skillsets 7, 8, and 9


#### Assignment Screenshots:

*Screenshot of P1 Validation:*

![P1 ERD](https://media.giphy.com/media/F1vsvfpakGZ0aCvRz1/giphy.gif)

#### GIF of Skillsets 7,8,9:

![Skillsets 7, 8, 9](https://media.giphy.com/media/P8K5NlB1qiYRgUFNan/giphy.gif)
